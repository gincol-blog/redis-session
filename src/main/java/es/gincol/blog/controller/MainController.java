package es.gincol.blog.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import es.gincol.blog.utils.Utils;

@RestController
public class MainController {

	private static final Logger log = LoggerFactory.getLogger(MainController.class);
	
	@Value("${spring.application.name}")
	private String appName;
	
	private String varSession = "listaNombresEnSession";

	@SuppressWarnings("unchecked")
	@PutMapping(path = "/addDataSession")
	public @ResponseBody String addDataSession(@RequestParam String name, HttpServletRequest request) {
		log.info("Grabando datos en session");
		HttpSession misession= request.getSession(true);
		List<String> nombres = (List<String>) misession.getAttribute(varSession);
		if(nombres!=null) {
			nombres.add(name);
		} else {
			nombres = new ArrayList<String>();
			nombres.add(name);
		}
		log.info("Add dato en session: " + name);
		misession.setAttribute(varSession, nombres);
		return "Session grabada: " + misession.getId();
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping(path = "/getDataSession")
	public @ResponseBody List<String> getDataSession(HttpServletRequest request) {
		log.info("Consultando datos en session");
		HttpSession misession= (HttpSession) request.getSession();
		List<String> nombres = (List<String>) misession.getAttribute(varSession);
		return nombres;
	}

	@GetMapping("/fecha")
	public @ResponseBody String date(HttpServletRequest request) {
		log.info("Acceso al metodo rest 'fecha'");
		return "Hola desde el RestController en fecha <br><h2>"
				+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()) + " !!!<h2>" + "<hr/>" + "<h3>app: '"
				+ appName + "'</h3>" + "<p>host name / ip: <b>" + Utils.getIp() + "</b></p>" + "<p>Session: <b>"
				+ request.getSession().getId() + "</b></p>" + "<hr/><hr/>";
	}

}
